// stream.spec.ts
import type { Page } from "playwright-core";
import type {BeforeScenarioFn, AfterScenarioFn, Next, EventEmitter, ScenarioContext} from 'artillery';
import type { Test } from '@support/types'
import { scenario as streamScenario } from "@scenarios/stream.spec";

export const setup: BeforeScenarioFn = (context: ScenarioContext, eventEmitter: EventEmitter, next: Next) => {
    console.log('BeforeScenarioFn - setup');
    next();
}

export const teardown: AfterScenarioFn = (context: ScenarioContext, eventEmitter: EventEmitter, next: Next) => {
    console.log('AfterScenarioFn - teardown')
    next();
}

export async function loadtest(page: Page, vuContext: ScenarioContext, eventEmitter: EventEmitter, test: Test) {
    const { step } = test;
    // console.log('vuContext')
    // console.log(JSON.stringify(vuContext, null, 2))
    await streamScenario(page);
    await page.waitForTimeout(3000);  // linger
}
