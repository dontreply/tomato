// support/types.d.ts
export type AsyncCallback = () => Promise<void>;
export type Step = (stepName: string, stepFn: AsyncCallback) => void;
export type Test = { step: Step }