// util.ts
export function randomBetween(min: number, max: number): number {
    const _min = Math.min(min, max);
    const _max = Math.max(min, max);
    return Math.random() * (_max - _min + 1) + _min; // min and max included
}

export function randomIntBetween(min: number, max: number): number {
    return Math.floor(randomBetween(min, max));
}

export function randomItem<T>(arr: Array<T>): T {
    return arr[Math.floor(Math.random() * arr.length)];
}

export function randomString(
    length: number,
    charset = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
) {
    let res = '';
    let _lenght: number = Math.floor(length);
    while (_lenght--) res += charset[(Math.random() * charset.length) | 0];
    return res;
}
