## Check artillery-engine-playwright Dockerfile for required playwright version
## github.com/artilleryio/artillery/blob/main/packages/artillery-engine-playwright/Dockerfile
FROM mcr.microsoft.com/playwright:v1.39.0-jammy as build
COPY --from=docker.io/artilleryio/artillery:2.0.3 /home/node/artillery /artillery
ENV PATH="/artillery/bin:${PATH}"
RUN chmod -R 777 /artillery && \
    mv /artillery/bin/run /artillery/bin/artillery && \
    rm -rf /root/.cache && \
    rm -rf /ms-playwright/firefox* && \
    rm -rf /ms-playwright/webkit*
