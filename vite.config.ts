// vite.config.ts
import path from "path";
import { defineConfig, normalizePath } from "vite";
import type { UserConfig } from "vite";
import pkg from './package.json' assert { type: 'json' }


const viteConfig: UserConfig = {
    publicDir: normalizePath(path.resolve(__dirname, './resources')),
    resolve: {
        alias: {
            "@pages": normalizePath(path.resolve(__dirname, './src/pages')),
            "@support": normalizePath(path.resolve(__dirname, './src/support')),
            "@scenarios": normalizePath(path.resolve(__dirname, './src/scenarios')),
        },
    },
    build: {
        outDir: './dist',
        assetsDir: '',
        assetsInlineLimit: 0,
        target: 'esnext',
        minify: false,
        lib: {
            entry: normalizePath(path.resolve(__dirname, './src/loadtest.ts')),
            // name: "[name]",
            formats: ['cjs'],
            fileName: format => 'script.js',
        },
        rollupOptions: {
            output: {
                assetFileNames: (assetInfo) => {
                    return `assets/[name]-[hash][extname]`;
                },
                chunkFileNames: 'assets/[name]-[hash][extname]',
                exports: "named",
            } ,
            external: [
                /^node:.*/,                         // ignore node
                ...Object.keys(pkg.dependencies),   // Don't bundle dependencies
              ],
        },
    }
};

module.exports = defineConfig(viteConfig);