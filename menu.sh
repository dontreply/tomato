#! /usr/bin/env bash

# 
# Menu using `whiptail`
# Used to start and stop services defined in ./docker-compose.yml
# 

SCRIPT_DIR=$(dirname "${0}")
E2E_DIR="${SCRIPT_DIR}/src"
DEFAULT_COMPOSE_FILE="${SCRIPT_DIR}/docker-compose.yml"
# create a `realpath` $PWD which is used in docker-compose.yml
PWD="$(pwd)/${SCRIPT_DIR}"

### SELENIUM GRID ###

OPTIONS_SELENIUM_GRID=(
    "0" "Back"
    "1" "Start Selenium hub"
    "2" "Stop Selenium hub"
    "3" "Start Chrome nodes"
    "4" "Stop Chrome nodes"
    "5" "Start Edge nodes"
    "6" "Stop Edge nodes"
    "7" "Stop all nodes"
    "8" "Stop all"
)
LENGTH_OPTIONS_SELENIUM_GRID="${#OPTIONS_SELENIUM_GRID[@]}"
LENGTH_OPTIONS_SELENIUM_GRID=$((LENGTH_OPTIONS_DOCKER_PLAYWRIGHT > 10 ? 10 : LENGTH_OPTIONS_DOCKER_PLAYWRIGHT))

function FN_SELENIUM_VARIABLES() {
    MAX_SESSIONS=3;
    NUMBER_NODES=1;
    while [ 1 ]; do
        MAX_SESSIONS="$(whiptail \
            --cancel-button "Back" \
            --title "Amount of browsers sessions" \
            --inputbox "Number max concurrency per node\n(input>1)" \
            10 40 "${MAX_SESSIONS}" \
            3>&1 1>&2 2>&3
        )";
        if [ "${MAX_SESSIONS}" = "" ] || [ "${MAX_SESSIONS}" = "0" ]; then return 0; fi;
        NUMBER_NODES="$(whiptail \
            --cancel-button "Back" \
            --title "Total amount of nodes" \
            --inputbox "Total number spawned containers\n(input>1)" \
            10 40 "${NUMBER_NODES}" \
            3>&1 1>&2 2>&3
        )";
        if [ "${NUMBER_NODES}" = "" ] || [ "${NUMBER_NODES}" = "0" ]; then return 0; fi;
        break;
    done;
}

function FN_CASE_SELENIUM_GRID () {
    # case "${given choice}" in ...
    case "${1}" in
        "0")  # Back
            return 0;
            ;;
        "1")
            # Start Selenium Hub
            docker compose --file="${DEFAULT_COMPOSE_FILE}" up --detach selenium-hub;
            ;;
        "2")
            # Stop Selenium Hub
            docker compose --file="${DEFAULT_COMPOSE_FILE}" down selenium-hub;
            ;;
        "3")
            # Start Chrome Node
            FN_SELENIUM_VARIABLES;
            if [[ "${MAX_SESSIONS}" =~ ^[1-9]+$ ]] || [[ "${NUMBER_NODES}" =~ ^[1-9]+$ ]]; then
                export SE_NODE_MAX_SESSIONS="${MAX_SESSIONS}";
                docker compose --file="${DEFAULT_COMPOSE_FILE}" scale --no-deps node-chrome="${NUMBER_NODES}";
                unset SE_NODE_MAX_SESSIONS;
            fi
            ;;
        "4")
            # Stop Chrome Node
            docker compose --file="${DEFAULT_COMPOSE_FILE}" down node-chrome;
            ;;
        "5")  # Start Edge Node
            FN_SELENIUM_VARIABLES;
            if [[ "${MAX_SESSIONS}" =~ ^[1-9]+$ ]] || [[ "${NUMBER_NODES}" =~ ^[1-9]+$ ]]; then
                export SE_NODE_MAX_SESSIONS="${MAX_SESSIONS}";
                docker compose --file="${DEFAULT_COMPOSE_FILE}" scale --no-deps node-edge="${NUMBER_NODES}";
                unset SE_NODE_MAX_SESSIONS;
            fi
            ;;
        "6")  # Stop Edge Node
            docker compose --file="${DEFAULT_COMPOSE_FILE}" down node-edge;
            ;;
        "7")  # Stop all browsers
            docker compose --file="${DEFAULT_COMPOSE_FILE}" down node-edge node-chrome;
            ;;
        "8")  # Stop all
            docker compose --file="${DEFAULT_COMPOSE_FILE}" down node-edge node-chrome selenium-hub;
            ;;
        *)
            echo "case UNKNOWN";
            exit 1;
            ;;
    esac;
    return 1; # SUCCES
}

### DOCKER PLAYWRIGHT ###

OPTIONS_DOCKER_PLAYWRIGHT=(
    "0" "Back"
    "1" "Run Docker Playwright"
    "2" "Run Docker Playwright test"
    "3" "Start Docker Playwright UI"
    "4" "Start Docker Playwright trace"
    "5" "Start Docker Playwright report"
    "6" "Start Docker Playwright merge reports"
    "7" "Run Docker Playwright Grid"
    "8" "Start Docker Playwright Grid UI"
    "9" "Start Docker Playwright Grid test"
)
LENGTH_OPTIONS_DOCKER_PLAYWRIGHT="${#OPTIONS_DOCKER_PLAYWRIGHT[@]}"
LENGTH_OPTIONS_DOCKER_PLAYWRIGHT=$(($LENGTH_OPTIONS_SELENIUM_GRID > 10 ? 10 : $LENGTH_OPTIONS_SELENIUM_GRID))

function FN_CASE_DOCKER_PLAYWRIGHT () {
    # case "${given choice}" in ...
    case "${1}" in
        "0")  # Back
            return 0;
            ;;
        "1")  # Run Docker Playwright
            docker compose --file="${DEFAULT_COMPOSE_FILE}" run --rm --interactive --service-ports playwright;
            ;;
        "1")  # Run Docker Playwright test
            docker compose --file="${DEFAULT_COMPOSE_FILE}" run --rm --interactive --service-ports playwright test;
            ;;
        "3")  # Start Docker Playwright UI
            PORT="9321"
            echo "http://127.0.0.1:${PORT}"
            xdg-open "http://127.0.0.1:${PORT}" || true;
            docker compose --file="${DEFAULT_COMPOSE_FILE}" run --rm --interactive --service-ports playwright \
                yarn playwright test --ui --ui-host=0.0.0.0 --ui-port=${PORT};
            ;;
        "4")  # Start Docker Playwright trace
            PORT="9322"
            echo "http://127.0.0.1:${PORT}"
            xdg-open "http://127.0.0.1:${PORT}" || true;
            docker compose --file="${DEFAULT_COMPOSE_FILE}" run --rm --interactive --service-ports playwright \
                yarn playwright show-trace --host=0.0.0.0 --port=${PORT};
            ;;
        "5")  # Start Docker Playwright report
            PORT="9323"
            echo "http://127.0.0.1:${PORT}"
            xdg-open "http://127.0.0.1:${PORT}" || true;
            docker compose --file="${DEFAULT_COMPOSE_FILE}" run --rm --interactive --service-ports playwright \
                yarn playwright show-report --host=0.0.0.0 --port=${PORT};
            ;;
        "6")  # Start Docker Playwright merge reports
            docker compose --file="${DEFAULT_COMPOSE_FILE}" run --rm --interactive --service-ports playwright \
                yarn playwright merge-reports --config=./merge.config.ts ./test-results/blob-results;
            ;;
        "7")  # Run Docker Playwright Grid
            docker compose --file="${DEFAULT_COMPOSE_FILE}" run --no-deps --rm --interactive --service-ports playwright-grid;
            ;;
        "8")  # Start Docker Playwright Grid UI
            PORT="9321"
            echo "http://127.0.0.1:${PORT}"
            xdg-open "http://127.0.0.1:${PORT}" || true;
            docker compose --file="$(pwd)/docker-compose.yml" run --no-deps --rm --interactive --service-ports playwright-grid \
                yarn playwright test --ui --ui-host=0.0.0.0 --ui-port=${PORT};
            ;;
        "9")  # Start Docker Playwright Grid test
            docker compose --file="$(pwd)/docker-compose.yml" run --no-deps --rm --interactive --service-ports playwright-grid \
                yarn playwright test;
            ;;
        *)
        echo "case UNKNOWN"
        exit 1;
        ;;
    esac;
    return 1; # SUCCES
}

### LOCAL PLAYWRIGHT ###

OPTIONS_LOCAL_PLAYWRIGHT=(
    "0" "Back"
    "1" "Run Playwright UI"
    "2" "Run Playwright test"
    "3" "Run Playwright test headed"
    "4" "Run Playwright test debug-mode"
    "5" "Run Playwright trace"
    "6" "Run Playwright report"
)
LENGTH_OPTIONS_LOCAL_PLAYWRIGHT="${#OPTIONS_LOCAL_PLAYWRIGHT[@]}"
LENGTH_OPTIONS_LOCAL_PLAYWRIGHT=$((LENGTH_OPTIONS_LOCAL_PLAYWRIGHT > 10 ? 10 : LENGTH_OPTIONS_LOCAL_PLAYWRIGHT))

function FN_CASE_LOCAL_PLAYWRIGHT () {
    case "${1}" in
        "0")  # Back
            return 0;
            ;;
        "1")  # Run Playwright UI
            PORT="9321"
            echo "http://127.0.0.1:${PORT}"
            xdg-open "http://127.0.0.1:${PORT}" || true;
            yarn --cwd="${E2E_DIR}" playwright test --ui --ui-port=${PORT};
            ;;
        "2")  # Run Playwright test
            yarn --cwd="${E2E_DIR}" playwright test;
            ;;
        "3")  # Run Playwright test headed
            yarn --cwd="${E2E_DIR}" playwright test --headed;
            ;;  
        "4")  # Run Playwright test debug-mode
            yarn --cwd="${E2E_DIR}" playwright test --debug;
            ;;
        "5")  # Run Playwright trace
            PORT="9322"
            echo "http://127.0.0.1:${PORT}"
            xdg-open "http://127.0.0.1:${PORT}" || true;
            yarn --cwd="${E2E_DIR}" playwright test;
            yarn --cwd="${E2E_DIR}" playwright show-trace --host=0.0.0.0 --port=${PORT};
            ;;
        "6")  # Run Playwright report"     
            PORT="9323"
            echo "http://127.0.0.1:${PORT}"
            xdg-open "http://127.0.0.1:${PORT}" || true;
            yarn --cwd="${E2E_DIR}" playwright show-report --host=0.0.0.0 --port=${PORT};
            ;;
        *)
            echo "case UNKNOWN"
            exit 1;
            ;;
    esac;
    return 1; # SUCCES
}      

### MAIN OPTIONS ###

# "${CHOICE}" "option name"
OPTIONS_MAIN=(
    "0" "Quit"
    "1" "Selenium Grid"
    "2" "Docker Playwright"
    "3" "Local Playwright"
)
LENGTH_OPTIONS_MAIN="${#OPTIONS_MAIN[@]}"
LENGTH_OPTIONS_MAIN=$((LENGTH_OPTIONS_MAIN > 10 ? 10 : LENGTH_OPTIONS_MAIN))

function FN_CASE_MAIN () {
    case "${1}" in
        "0")  # Quit
            exit 0;
        ;;
        "1")  # Selenium Grid
            CHOICE="$(
                whiptail \
                    --title "Selenium Grid" \
                    --menu "Make your choice" \
                    20 90 "${LENGTH_OPTIONS_SELENIUM_GRID}" \
                    "${OPTIONS_SELENIUM_GRID[@]}" \
                    3>&2 2>&1 1>&3
            )";
            FN_CASE_SELENIUM_GRID "${CHOICE}";
            FN_RESULT="${?}";  # 1 = success, 0 = back
            if [ "${FN_RESULT}" -ge 1 ]; then
                exit 0;
            fi
            ;;
        "2")  # Docker Playwright
            CHOICE="$(
                whiptail \
                    --title "Docker Playwright" \
                    --menu "Make your choice" \
                    20 90 "${LENGTH_OPTIONS_DOCKER_PLAYWRIGHT}" \
                    "${OPTIONS_DOCKER_PLAYWRIGHT[@]}" \
                    3>&2 2>&1 1>&3
            )";
            FN_CASE_DOCKER_PLAYWRIGHT "${CHOICE}";
            FN_RESULT="${?}";  # 1 = success, 0 = back
            if [ "${FN_RESULT}" -ge 1 ]; then
                exit 0;
            fi
            ;;
        "3")  # Local Playwright
            CHOICE="$(
                whiptail \
                    --title "Docker Playwright" \
                    --menu "Make your choice" \
                    20 90 "${LENGTH_OPTIONS_LOCAL_PLAYWRIGHT}" \
                    "${OPTIONS_LOCAL_PLAYWRIGHT[@]}" \
                    3>&2 2>&1 1>&3
            )";
            FN_CASE_LOCAL_PLAYWRIGHT "${CHOICE}";
            FN_RESULT="${?}";  # 1 = success, 0 = back
            if [ "${FN_RESULT}" -ge 1 ]; then
                exit 0;
            fi
            ;;
        *)
            echo "case UNKNOWN";
            exit 1;
            ;;
    esac
}

### MAIN ###

function FN_MAIN () {
    while [ 1 ]; do
        CHOICE=$(
            whiptail \
                --title "Options" \
                --menu "Make your choice" \
                20 90 "${LENGTH_OPTIONS_MAIN}" \
                "${OPTIONS_MAIN[@]}" \
                3>&2 2>&1 1>&3	
        );
        FN_CASE_MAIN "${CHOICE}";
        FN_RESULT="${?}";  # 1 = success, 0 = back
        if [ "${FN_RESULT}" -ge 1 ]; then
            exit 0;
        fi
    done;
}

### START ###

FN_MAIN;
exit 0;
